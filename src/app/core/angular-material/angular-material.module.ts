import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatListModule, MatIconModule} from '@angular/material';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
      MatListModule,
      MatIconModule
  ]
})
export class AngularMaterialModule { }
