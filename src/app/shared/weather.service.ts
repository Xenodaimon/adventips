import { Injectable } from '@angular/core';
import {BehaviorSubject, from, Observable} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';

export class WeatherPrevision {
  public date: string;
  public meteo: string;
  public portance: string;
  public repiquage: string;
  public precipitationPrevue: number;
}

const mockData = [{
  date: '2019-08-31T00:00:00+00:00',
  portance: 'yes',
  meteo: 'pluie',
  repiquage: 'no',
  precipitationPrevue: 15.0
}, {
  date: '2019-09-01T00:00:00+00:00',
  portance: 'no',
  meteo: 'soleil',
  repiquage: 'no',
  precipitationPrevue: 17.0
}, {
  date: '2019-09-02T00:00:00+00:00',
  portance: 'no',
  meteo: 'soleil',
  repiquage: 'no',
  precipitationPrevue: 20.0
}, {
  date: '2019-09-03T00:00:00+00:00',
  portance: 'yes',
  meteo: 'soleil',
  repiquage: 'no',
  precipitationPrevue: 23.0
},{
  date: '2019-09-04T00:00:00+00:00',
  portance: 'yes',
  meteo: 'pluie',
  repiquage: 'no',
  precipitationPrevue: 8.0
},{
  date: '2019-09-05T00:00:00+00:00',
  portance: 'no',
  meteo: 'nuage',
  repiquage: 'no',
  precipitationPrevue: 11.0
},{
  date: '2019-09-06T00:00:00+00:00',
  portance: 'no',
  meteo: 'nuage',
  repiquage: 'no',
  precipitationPrevue: 18.0
},{
  date: '2019-09-07T00:00:00+00:00',
  portance: 'no',
  meteo: 'nuage',
  repiquage: 'no',
  precipitationPrevue: 25.0
}];

const API_ENDPOINT = 'https://hacktafermeapi20190831102109.azurewebsites.net/api';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private weatherPrevisions$: BehaviorSubject<WeatherPrevision[]> = new BehaviorSubject(Array<WeatherPrevision>());
  public readonly weatherPrevisions: Observable<WeatherPrevision[]> = from(this.weatherPrevisions$);

  constructor(private http: HttpClient) { }

  public getPrevisionForSite(): void {
    const headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origin', '*');
    this.http.get(`${API_ENDPOINT}/exploitations/1/previsions`, {headers}).subscribe(data => {
      console.log('Received:', data);
      this.weatherPrevisions$.next(data as Array<WeatherPrevision>);
    }, err => {
      console.error(err);
      const error = err as HttpErrorResponse;
      if (error.status === 0 && error.statusText === 'Unknown Error') {
        this.weatherPrevisions$.next(mockData as Array<WeatherPrevision>);
      }
    });
  }
}
