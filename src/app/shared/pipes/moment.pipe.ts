import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'moment'
})
export class MomentPipe implements PipeTransform {
  constructor() {
    moment.locale('fr');
  }

  transform(value: string | Date, ...args: any[]): string {
    return moment(value).format('ddd D MMM').toString();
  }

}
