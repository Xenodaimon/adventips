import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  constructor(private navCtrl: NavController) { }

  ngOnInit() {}

  login(form: NgForm): void {
    console.log('Trying to log in with:', form.value);
    this.navCtrl.navigateRoot('/').then((data, ...args) => {
      console.log('L', data, args);
    }).catch(err => {
      console.error(err);
    });
  }
}
