import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule} from '@angular/forms';

import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {IonicModule} from '@ionic/angular';
import {JwtModule, JwtModuleOptions} from '@auth0/angular-jwt';
import {AppRoutingModule} from '../app-routing.module';

const JWTModuleOptions: JwtModuleOptions = {
  config: {
//    tokenGetter: yourTokenGetter,
//    whitelistedDomains: yourWhitelistedDomains
  }
};

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    AppRoutingModule,
    JwtModule.forRoot(JWTModuleOptions)
  ]
})
export class AuthModule { }
