import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs.router.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { TabsPage } from './tabs.page';

import { AngularMaterialModule } from '../core/angular-material/angular-material.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    FontAwesomeModule,
      AngularMaterialModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
