import { Component } from '@angular/core';
import {WeatherPrevision, WeatherService} from '../shared/weather.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  private dayPrevisions: Array<WeatherPrevision>;

  constructor(private weatherService: WeatherService) {
    this.weatherService.weatherPrevisions.subscribe(data => {
      this.dayPrevisions = data;
    });

    this.weatherService.getPrevisionForSite();
  }
}
