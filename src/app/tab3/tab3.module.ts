import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab3Page } from './tab3.page';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

import {AngularMaterialModule} from '../core/angular-material/angular-material.module';
import {MatExpansionModule, MatListModule} from '@angular/material';
import { MomentPipe } from '../shared/pipes/moment.pipe';
import {TabHeaderComponent} from '../tab-header/tab-header.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        AngularMaterialModule,
        RouterModule.forChild([{path: '', component: Tab3Page}]),
        FontAwesomeModule,
        MatListModule,
        MatExpansionModule
    ],
    exports: [
        MomentPipe
    ],
    declarations: [Tab3Page, MomentPipe, TabHeaderComponent]
})
export class Tab3PageModule {}
